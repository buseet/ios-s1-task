# Buseet - iOS Coding task #


### Idea ###
Image search and browsing using Flicker.

### Highlevel Overview ###
The app has two screens, Home and Favorites.

#### 1 - Home Screen: ####

- User can search for images using search bar.
- User can browse images with infinte scrolling.
- User can tap on image to view it in full screen.
- User can add/remove images to Favorites.

#### 2 - Favorites Screen: ####

- User can browse all favorites images with infinte scrolling.
- User can unfavorite an image.
- All images in Favorite screen can be shown offline without internet connection.

### Implementation ###

- The app must use Rx.
- Use MVVM + Coordinator.
- The app must handle all error and loading states.
- The app must include UnitTests.
- Feel free to use any third-party library that helps you complete this task, but be ready to explain why you decided to use it.

### Bonus points ###

- Integrate CI/CD tool to the project.
- Use SwiftUI.

### ATTENTION! ###
Please don't share this document neither with anybody nor to any public website.

### Submit your challenge ###
- Create a new repo
- Push your commits to it as you would do in a real-world task
- Send us an email with the repo link

*NOTE: This challenge is aimed to senior developers, so expected to hit a high point of technicality.*

Good luck!